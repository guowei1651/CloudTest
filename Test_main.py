#! /bin/python
__author__ = 'guow'

from io import BytesIO
from docker import *

if __name__ == "__main__":
    dockerfile = '''
        # Shared Volume
        FROM centos-guow
        MAINTAINER first last, first.last@yourdomain.com
        CMD ["/bin/sh"]
        '''
    #cli = Client(base_url='unix://var/run/docker.sock',version="1.18")
    cli = Client(base_url='tcp://172.31.5.200:4243',version="1.18")

    #images_list = cli.search("centos")
    #for image in images_list:
    #    print image['name']

    #f = BytesIO(dockerfile.encode('utf-8'))
    #build_step = cli.build(fileobj=f, rm=True, tag='guow-api-test')
    #for line in build_step:
    #    print line


    #stdout_and_stderr = cli.attach("7012520abacc")
    #for line in stdout_and_stderr:
    #    print line

    #commitresult = cli.commit("28bbc7242f92",repository="guow-api-commit-test",author='guow')
    #print commitresult

    #file = cli.copy("28bbc7242f92", "/home/DRO/Test_main.py")
    #for line in file:
    #    print line

    #diff = cli.diff("28bbc7242f92")
    #for d in diff:
    #    print d

    #docker_event = cli.events()
    #for de in docker_event:
    #    print de

    #exec_obj = cli.exec_create("28bbc7242f92",'echo "hello world"',stdout=True)
    #print "exec_obj"
    #print exec_obj

    #result = cli.exec_inspect(exec_obj['Id'])
    #print result

    #response = cli.exec_start(exec_obj['Id'], stream=True)
    #for line in response:
    #    print line

    #image_tar = cli.get_image("guow-api-commit-test")
    #print image_tar

    #history = cli.history("guow-api-commit-test")
    #print history

    #images_list = cli.images()
    #print images_list

    #print cli.info()
    print cli.version()

    container = cli.create_container(
        image='ci-build',
        #command='ls /root/xiaoxu/',
        command='/root/CI/CloudTest.sh',
        volumes = "/root/xiaoxu/:/root",
        detach=True,
        ports="")
    #print(container)

    containerID=container.get('Id')

    #tarout = cli.export(containerID)
    #print tarout

    #stats_obj = cli.stats(containerID)
    #for stat in stats_obj:
    #   print(stat)

    response = cli.start(containerID)
    #print(response)

    cli.commit(containerID)

    #response = cli.stats(containerID)
    #print(response)

    while True:
        inspect = cli.inspect_container(containerID)
        #print inspect
        if inspect['State']['Running'] is not True:
            break
        import time
        time.sleep(1)
        print "wait finish!"

    log = cli.logs(containerID)
    print(log)

    try:
        res = cli.stop(containerID)
        #print (res)
    except :
        try :
            cli.kill(containerID)
        except:
            pass

    cli.remove_container(containerID)

    #print cli.containers()

    exit(1)